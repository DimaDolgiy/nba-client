/**
 * Created by dima on 02.11.18.
 */

export class Team {
  id: number;
  team_id: string;
  full_name: string;
  city: string;
  state: string;
  division: string;
}
