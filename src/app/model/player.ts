import {Team} from "./team";
/**
 * Created by dima on 03.11.18.
 */

export class Player{
  id: number;
  firstName: string;
  lastName: string;
  phone: number;
  height: number;
  team: Team;
}
