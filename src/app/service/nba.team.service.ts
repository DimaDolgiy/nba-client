/**
 * Created by dima on 02.11.18.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class NbaTeamService {
  private baseUrl = 'http://localhost:8080/api/nba';


  constructor(private http: HttpClient) { }

  getTeams(): Observable<any> {
    return this.http.get(this.baseUrl + '/teams');
  }

}
