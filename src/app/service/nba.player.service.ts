/**
 * Created by dima on 04.11.18.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {Player} from "../model/player";

@Injectable()
export class NbaPlayerService {
  private baseUrl = 'http://localhost:8080/api/nba';


  constructor(private http: HttpClient) {
  }

  getPlayers(): Observable<any> {
    return this.http.get(this.baseUrl + '/players');
  }

  save(player: Player): Observable<any> {
    return this.http.post(this.baseUrl + '/players', player);
  }

  getPlayersByTeam(id: number) :  Observable<any>{
    return this.http.get(this.baseUrl + '/players/' + id);
  }
}
