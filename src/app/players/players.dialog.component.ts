/**
 * Created by dima on 03.11.18.
 */
import {Component, OnDestroy, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {Player} from "../model/player";
import {Subscription} from "rxjs/index";
import {Team} from "../model/team";
import {NbaPlayerService} from "../service/nba.player.service";
import {NbaTeamService} from "../service/nba.team.service";


@Component({
  selector: 'player-add',
  templateUrl: './players.dialog.component.html',
})

export class PlayersDialogComponent implements OnInit, OnDestroy{

  player: Player = new Player;
  teams: Team[];
  sub: Subscription;
  selectedValue: string;

  constructor(private nbaPlayerService: NbaPlayerService,
              private router: Router, private nbaTeamService: NbaTeamService){}

  ngOnInit(): void {
    this.nbaTeamService.getTeams().subscribe(data => this.teams = data);
  }

  save() {
    if(this.player){
      this.teams.forEach(team =>{
        if(team.team_id === this.selectedValue){
          this.player.team = team;
        }
      });
      this.nbaPlayerService.save(this.player).subscribe(result => {
      }, error => console.error(error));
      this.router.navigate(['/nba-players']);
    }
  }


  ngOnDestroy(): void {
    if(this.sub){
      this.sub.unsubscribe();
    }
  }
}
