/**
 * Created by dima on 03.11.18.
 */


import {Component, OnDestroy, OnInit} from '@angular/core';
import {Player} from "../model/player";
import {Observable, Subscription} from "rxjs/index";
import {NbaPlayerService} from "../service/nba.player.service";



@Component({
  selector: 'players-list',
  templateUrl: './players.component.html',
})

export class PlayersComponent implements OnInit , OnDestroy {

  players: Observable<Player[]>;
  sub: Subscription;
  constructor(private nbaService: NbaPlayerService) {}

  ngOnInit(): void {
    this.nbaService.getPlayers().subscribe((data) => this.players = data);
  }

  ngOnDestroy(): void {
    if(this.sub){
      this.sub.unsubscribe();
    }
  }
}

