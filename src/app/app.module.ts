import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule, MatTableModule,
 MatOptionModule, MatSelectModule, MatMenuModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {NbaTeamService} from './service/nba.team.service';
import {TeamComponent} from './team/team.component'
import {PlayersComponent} from './players/players.component';
import {PlayersDialogComponent} from './players/players.dialog.component';
import {NbaPlayerService} from "./service/nba.player.service";
import {TeamPLayersComponent} from "./team/team.players.component";

@NgModule({
  declarations: [
    AppComponent,
    TeamComponent,
    PlayersComponent,
    PlayersDialogComponent,
    TeamPLayersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatTableModule,
    MatOptionModule,
    MatSelectModule,
    MatMenuModule
  ],
  providers: [NbaPlayerService, NbaTeamService],
  bootstrap: [AppComponent]
})
export class AppModule { }
