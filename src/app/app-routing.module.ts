import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PlayersDialogComponent} from "./players/players.dialog.component";
import {AppComponent} from "./app.component";
import {TeamComponent} from "./team/team.component";
import {PlayersComponent} from "./players/players.component";
import {TeamPLayersComponent} from "./team/team.players.component";

const routes: Routes = [
  { path: '', redirectTo: '/nba-teams', pathMatch: 'full' },
  {
    path: 'nba-teams',
    component: TeamComponent
  },
  {
    path: 'nba-players',
    component: PlayersComponent
  },
  {
    path: 'nba-player-add',
    component: PlayersDialogComponent
  },
  {
    path: 'players/:id',
    component: TeamPLayersComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
