/**
 * Created by dima on 04.11.18.
 */


import { Subscription} from 'rxjs';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {NbaPlayerService} from '../service/nba.player.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Player} from "../model/player";

@Component({
  selector: 'nba-team-players',
  templateUrl: './team.players.component.html',
})

export class TeamPLayersComponent implements OnInit, OnDestroy{

  private id: number;
  sub: Subscription;
  players: Player[];

  constructor(private playerService: NbaPlayerService, private router: Router, private activateRoute: ActivatedRoute){
    this.sub = activateRoute.params.subscribe(params=>this.id=params['id']);
  }

  ngOnInit(): void {
    this.playerService.getPlayersByTeam(this.id).subscribe(data => this.players = data);

  }

  ngOnDestroy(): void {
    if(this.sub){
      this.sub.unsubscribe();
    }
  }
}
