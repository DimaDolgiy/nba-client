/**
 * Created by dima on 02.11.18.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Team} from '../model/team';
import {NbaTeamService} from '../service/nba.team.service';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'nba-team-list',
  templateUrl: './team.component.html',
})

export class TeamComponent implements OnInit , OnDestroy{

  teams: Observable<Team[]>;
  sub: Subscription;

  constructor(private nbaService: NbaTeamService) {}

  ngOnInit(): void {
    this.getTeams();
  }

  ngOnDestroy(): void {
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

  getTeams(): void {
    this.nbaService.getTeams().subscribe(data => this.teams = data);
  }
}
