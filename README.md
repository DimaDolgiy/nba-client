# NbaClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.4.

**1. Clone the application**

```bash
git clone https://DimaDolgiy@bitbucket.org/DimaDolgiy/nba-client.git
```
**2.Run the frontend app using npm**

```bash
install node.js on your system
```

```bash
cd nba-client
npm install
```

```bash
ng serve --open
```

Frontend server will run on <http://localhost:4200>
